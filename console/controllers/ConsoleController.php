<?php

namespace console\controllers;

use common\models\Course;
use yii\web\Controller;

class ConsoleController extends Controller
{

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $file = simplexml_load_file("http://www.cbr.ru/scripts/XML_daily.asp");

        Course::updateAll(['status' => Course::STATUS_OLD]);
        $course = [];

        $fmt = new \NumberFormatter('ru_RU', \NumberFormatter::DECIMAL);

        foreach ($file as $el) {


            $course[] = [
                'num_code' => (int)strval($el->NumCode),
                'char_code' => strval($el->CharCode),
                'nominal' => (int)strval($el->Nominal),
                'name' => strval($el->Name),
                'value' => $fmt->parse((string)$el->Value),
                'created_at' => date('Y-m-d H:i:s')

            ];
        }

        foreach ($course as $item) {

            $model = Course::find()
                ->where(['num_code' => $item['num_code']])
                ->orderBy(['id' => SORT_DESC])
                ->one();

            if (!$model) {
                $model = new Course();
            }

            if ($model->value == $item['value']) {
                continue;
            } else {
                $model = new Course();
            }


            $model->num_code = $item['num_code'];
            $model->char_code = $item['char_code'];
            $model->nominal = $item['nominal'];
            $model->name = $item['name'];
            $model->value = $item['value'];
            $model->created_at = date('Y-m-d H:i:s');
            $model->status = Course::STATUS_NEW;

            if (!$model->save()) {
                print_r('not save');
            }
        }
        print_r("complete \n");
    }

}