<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%course}}`.
 */
class m191220_093637_create_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('course', [
            'id' => $this->primaryKey(),
            'num_code' => $this->integer(),
            'char_code' => $this->string(),
            'nominal' => $this->integer(),
            'name' => $this->string(),
            'value' => $this->float(),
            'created_at' => $this->dateTime()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('course');
    }
}
