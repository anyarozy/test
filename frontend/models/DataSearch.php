<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Data;

/**
 * DataSearch represents the model behind the search form of `app\models\Data`.
 */
class DataSearch extends Data
{
    public $year;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'address_id'], 'integer'],
            [['card_number', 'date', 'service', 'year'], 'safe'],
            [['volume'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Data::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);


        if (!$this->validate() || !$params) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'volume' => $this->volume,
            'address_id' => $this->address_id,
        ]);
        $query->andFilterWhere(['like', 'card_number', trim($this->card_number)])
            ->andFilterWhere(['like', 'service', trim($this->service)]);

        if ($this->date) {
            $query->andFilterWhere(['between', 'date', $this->date . '-01 00:00:00', $this->date . '-31 23:59:59']);
        }
        if ($this->year) {
            $query->andFilterWhere(['between', 'date', $this->year . '-01-01 00:00:00', $this->year . '-12-01 00:00:00']);
        }

        return $dataProvider;
    }
}
