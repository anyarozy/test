<?php


use common\models\Course;
use miloschuman\highcharts\Highcharts;


$this->title = 'My Yii Application';

?>


<div class="site-index">

    <?php
    $chuck = array_chunk($charts, 5);
    foreach ($chuck as $item) :?>
        <?
        /** @var Course $item */
        echo Highcharts::widget([
            'options' => [
                'title' => ['text' => 'Курсы валют'],
                'yAxis' => [
                    'title' => ['text' => 'курс']
                ],
                'series' => $item
            ]
        ]); ?>

    <?php endforeach;


    ?>


</div>
