<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">

    <!---->
    <!--    <div class="row">-->
    <!--        <div class="col-lg-3">-->
    <!---->
    <!--            --><? // foreach ($panel as $k => $item): ?>
    <!---->
    <!--                <div>-->
    <!--                    <a href="-->
    <? //= \yii\helpers\Url::to(['/site/index', 'DataSearch[year]' => $k])?><!--">-->
    <!--                        --><? //= $k ?>
    <!--                    </a>-->
    <!--                    (--><? //= $item['count'] ?><!--)</div>-->
    <!--            -->
    <!--                --><? // foreach ($item['month'] as $key => $i) : ?>
    <!--                -->
    <!--                    <div style="margin-left: 20px">-->
    <!--                        <a href="-->
    <? //= \yii\helpers\Url::to(['/site/index', 'DataSearch[date]' => Yii::$app->formatter->asDate($i['date'], "Y-M")])?><!--">-->
    <!--                            --><? //= Yii::$app->formatter->asDate($i['date'], "php:M"); ?>
    <!--                        </a>(--><? //= $i['count'] ?><!--)-->
    <!--                    </div>-->
    <!---->
    <!--                --><? // endforeach; ?>
    <!--            --><? // endforeach; ?>
    <!---->
    <!--        </div>-->
    <!--        <div class="col-lg-9">-->
    <!---->
    <!---->
    <!--            --><? // Pjax::begin(); ?>
    <!---->
    <!--            --><? //= GridView::widget([
    //                'dataProvider' => $dataProvider,
    //                'filterModel' => $searchModel,
    //                'columns' => [
    //                    ['class' => 'yii\grid\SerialColumn'],
    //
    //                    [
    //                        'attribute'=>'id',
    //                        'filter' => false,
    //                    ],
    //                    [
    //                        'attribute'=>'card_number',
    //                    ],
    //                    [
    //                        'attribute'=>'date',
    //                        'filter' => false,
    //                    ],
    //
    //                    [
    //                        'attribute'=>'volume',
    //                        'filter' => false,
    //                    ],
    //                    [
    //                        'attribute'=>'service',
    //                        'filter' => false,
    //                    ],
    //                    [
    //                        'attribute'=>'address_id',
    //                        'filter' => false,
    //                    ],
    //                ],
    //            ]); ?>
    <!---->
    <!---->
    <!--            --><?php //Pjax::end() ?>
    <!---->
    <!--        </div>-->


    <!--    </div>-->
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.
    </p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'subject') ?>

                <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
