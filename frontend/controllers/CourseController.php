<?php

namespace frontend\controllers;


use common\models\Course;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;

/**
 * Class CourseController
 * @package frontend\controllers
 */
class CourseController extends ActiveController
{

    public $modelClass = Course::class;

    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::class,
        ];
        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['delete'], $actions['create'], $actions['view'], $actions['update']);

        $actions['index'] = [
            'class' => 'yii\rest\IndexAction',
            'modelClass' => $this->modelClass,
            'prepareDataProvider' => [$this, 'prepareDataProvider'],
        ];

        return $actions;
    }

    public function prepareDataProvider()
    {
        return new ActiveDataProvider(
            [
                'query' => Course::find()->where(['status' => Course::STATUS_NEW])
            ]
        );
    }

    public function actionStat($dateFrom = null, $dateTo = null, $currency = null)
    {
        $query = Course::find();

        if ($dateFrom = $this->checkDate($dateFrom)) {
            $query->andWhere(['>=', 'created_at', $dateFrom]);
        }
        if ($dateTo = $this->checkDate($dateTo)) {
            $query->andWhere(['<=', 'created_at', $dateTo]);
        }

        if ($currency) {
            $query->andWhere(['char_code' => $currency]);
        }

        return $query->all();
    }

    private function checkDate($date)
    {
        if ($date) {
            try {
                $dateObj = \DateTime::createFromFormat('Y-m-d', $date);

                if ($date === $dateObj->format('Y-m-d')) {
                    return $dateObj->format('Y-m-d');
                }

            } catch (ErrorException $e) {
                throw new Exception ('Формат даты должен быть YYYY-MM-DD');
            }
        }
    }

}