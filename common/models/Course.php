<?php

namespace common\models;


/**
 * This is the model class for table "course".
 *
 * @property int $id
 * @property int|null $num_code
 * @property int|null $status
 * @property string|null $char_code
 * @property int|null $nominal
 * @property string|null $name
 * @property string|null $created_at
 * @property float|null $value
 */
class Course extends \yii\db\ActiveRecord
{

    const STATUS_NEW = 1;
    const STATUS_OLD = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['num_code', 'nominal', 'status'], 'integer'],
            [['value'], 'number'],
            [['char_code', 'name'], 'string', 'max' => 255],
            [['created_at'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'num_code' => 'Num Code',
            'char_code' => 'Char Code',
            'nominal' => 'Nominal',
            'name' => 'Name',
            'value' => 'Value',
            'created_at' => 'Date',
        ];
    }

}
